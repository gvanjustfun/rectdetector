public class Rect {

    private Point p1;
    private Point p2;
    private Point p3;
    private Point p4;

    public Rect(Point p1, Point p2, Point p3, Point p4) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }

    /*
    *The method checks if holded points represent a rectangle.
    *We check that two opposite sides or diagonals are equal 
    *and that diagonal with two different sides satisfy 
    *the Pythagoras theory. 
    *We don't know the line between two points is a side or
    *a diagonal of rectangle. We accept this fact, because 
    *equality of two opposite sides and diagonals are enougth
    *for us.
    */
    public boolean isRectangle() {
        if (p1.equals(p2) || p2.equals(p3) || p3.equals(p4) || p4.equals(p1)) {
            return false;
        }

        double d12 = getDistance(p1, p2);
        double d23 = getDistance(p2, p3);
        double d34 = getDistance(p3, p4);
        double d41 = getDistance(p4, p1);

        double d13 = getDistance(p1, p3);
        double d24 = getDistance(p2, p4);
        boolean checkPyth1 = checkPythagoras(d13, d12, d23);
        boolean checkPyth2 = checkPythagoras(d24, d34, d41);

        return d12 == d34 && d23 == d41 && checkPyth1 && checkPyth2;
    }

    /*
    *The method finds a distance between two points
    *using Pythagoras theory
    */
    private double getDistance(Point p1, Point p2) {
        int catheter1 = Math.abs(p1.getX() - p2.getX());
        int catheter2 = Math.abs(p1.getY() - p2.getY());
        return Math.sqrt(catheter1 * catheter1 + catheter2 * catheter2);
    }

    /*
    *The method checks Pythagoras theory for three distances.
    *At first we find a biggest distance between three 
    *provided distances and declare it as hyposenuse. 
    *Then we find two smaller distances and declare 
    *it as catheters.  
    */
    private boolean checkPythagoras(double d13, double d12, double d23) {
        double hypothenuse = d13 >= d12 && d13 >= d23 ? d13 : d12 >= d23 ? d12 : d23;
        double catheter1 = d13 == hypothenuse ? d12 : d13;
        double catheter2 = d23 == hypothenuse ? d12 : d23;
        double leftSide = Math.round(hypothenuse * hypothenuse);
        double rightSide = Math.round((catheter1 * catheter1) + (catheter2 * catheter2));
        return leftSide == rightSide;
    }

    public Point getP1() {
        return p1;
    }

    public Point getP2() {
        return p2;
    }

    public Point getP3() {
        return p3;
    }

    public Point getP4() {
        return p4;
    }

    @Override
    public String toString() {
        return String.format("[%s, %s, %s, %s]", p1, p2, p3, p4);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        } else {
            Rect rect = (Rect) obj;
            return rect.getP1().equals(p1) && rect.getP2().equals(p2) && rect.getP3().equals(p3)
                    && rect.getP4().equals(p4);
        }
    }

}