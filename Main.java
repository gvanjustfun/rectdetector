import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Point> points = new ArrayList<>();

        points.add(new Point(1, 1));
        points.add(new Point(4, 4));
        points.add(new Point(1, 4));
        points.add(new Point(4, 1));
        points.add(new Point(7, 1));
        points.add(new Point(7, 4));

        points.add(new Point(4, 4));
        points.add(new Point(11, 3));
        points.add(new Point(5, 6));
        points.add(new Point(10, 1));
        points.add(new Point(2, 5));
        points.add(new Point(3, 7));

        points.add(new Point(2, 1));
        points.add(new Point(1, 2));
        points.add(new Point(0, 1));
        points.add(new Point(1, 0));

        List<Rect> rectangles = RectDetector.findRectangles(points);
        System.out.println(String.format("Rectangles cout: %s", rectangles.size()));
    }

}