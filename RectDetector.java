import java.util.ArrayList;
import java.util.List;

public class RectDetector {

     /*
    *This method finds rectangles from list of points.
    *At first step we create a new list without duplicates.
    *Then we iterate the list for check unique combinations 
    *of four points.
    */
    public static List<Rect> findRectangles(List<Point> pointsInput) {
        List<Point> points = new ArrayList<>();
        for(Point p : pointsInput) {
            if(!points.contains(p)) {
                points.add(p);
            }
        }

        List<Rect> rectangles = new ArrayList<Rect>();

        for (int i = 0; i < points.size(); i++) {
            for (int j = i + 1; j < points.size(); j++) {
                for (int k = j + 1; k < points.size(); k++) {
                    for (int l = k + 1; l < points.size(); l++) {
                        Point p1 = points.get(i);
                        Point p2 = points.get(j);
                        Point p3 = points.get(k);
                        Point p4 = points.get(l);

                        Rect rect = new Rect(p1, p2, p3, p4);
                        if (rect.isRectangle() && !rectangles.contains(rect)) {
                            rectangles.add(rect);
                            System.out.println(rect);
                        }
                    }
                }
            }
        }

        return rectangles;
    }

}